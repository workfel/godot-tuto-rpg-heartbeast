extends TileMap


var instanceCraft
var lastCellPos
var lastCellValueId

func _input(event):
	if instanceCraft:
		if event is InputEventMouseButton:
			var tilePostGlobal = self.world_to_map(self.get_global_mouse_position())
			lastCellPos = tilePostGlobal
			lastCellValueId = get_cellv(lastCellPos)
			set_cellv(tilePostGlobal, 0);
			instanceCraft = null
			WorldLoadingGeneration.save("Tree", 0, tilePostGlobal.x, tilePostGlobal.y)
		if event is InputEventMouseMotion:
			if lastCellPos:
				set_cellv(lastCellPos, lastCellValueId);
			var tilePostGlobal = self.world_to_map(self.get_global_mouse_position())
			lastCellPos = tilePostGlobal
			lastCellValueId = get_cellv(lastCellPos)
			set_cellv(tilePostGlobal, 0);

		
func place_item(item):
	instanceCraft = item;


func _on_BtnCraftTarp_craft_item(item):
	place_item(item)
