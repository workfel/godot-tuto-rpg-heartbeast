extends Resource

class_name Item

export(String) var item_type = "Wood"
export(int) var count = 1
export(Image) var icon = null

