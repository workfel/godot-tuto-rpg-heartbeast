extends Button


export(Array , String) var itemsRequired = []
export(Array , int) var itemsRequiredCount = []
export(String) var lootName


const ItemDrop = preload("res://ItemDrop.tscn")

var instanceCraft = null

signal craft_item(craftTscn)

func _ready():
	self.disabled = true
	

func _on_BtnCraftTarp_visibility_changed():
	var canCraft = false
	var i =0
	
	for item in itemsRequired:
		canCraft =PlayerInventory.has_count_of_items(item, itemsRequiredCount[i])
		i +=1
		
	if canCraft:
		self.disabled = false
	
		


func _on_BtnCraftTarp_pressed():
		instanceCraft = ItemDrop.instance()
#		instanceCraft.get_node("Sprite").texture = load("res://Crafts/" + self.icon + ".png")
		instanceCraft.get_node("Sprite").texture = self.icon
		emit_signal("craft_item", instanceCraft)
#		CraftManager.place_item(instanceCraft)
#		var main = get_tree().current_scene
#		main.add_child(instanceCraft)
#		print(get_global_mouse_position())
#		print(get_local_mouse_position());
#		var local_position = my_tilemap.to_local(get_global_mouse_position())
#		var map_position = my_tilemap.world_to_map(local_position)
#		instanceCraft.global_position = get_local_mouse_position()
		
		
