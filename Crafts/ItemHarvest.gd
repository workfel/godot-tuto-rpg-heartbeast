extends StaticBody2D


export(int) var heatlh = 0
export(String) var itemTypeRequired = null
#export(Texture) var loot = null;
export(String) var loot = '';
export(int) var nbLoot = 1;

const ItemDrop = preload("res://ItemDrop.tscn")
const RangeLootPosition = 10;


func _ready():
	pass # Replace with function body.

func item_hit():
	computeHealth()

func item_died():
	instanciate_loots()
	

func instanciate_loots():
		for n in nbLoot:
			var item = ItemDrop.instance()
			item.get_node("Sprite").texture = load("res://Crafts/" + loot + ".png")
			
			var main = get_tree().current_scene
			main.add_child(item)
			item.global_position.x = global_position.x + lerp(-RangeLootPosition * n, RangeLootPosition +1, randf())
			item.global_position.y = global_position.y + lerp(-RangeLootPosition * n, RangeLootPosition +1, randf())

func computeHealth():
	heatlh -= 1
	if(heatlh ==0):
		item_died()
		queue_free()
