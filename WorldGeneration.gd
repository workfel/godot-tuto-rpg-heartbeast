extends Node2D

onready var dirt_tilemap = $DirtPathTM
onready var wall_tilemap = $DirtCliffTM
onready var grass_tilemap = $GrassTileMap
onready var water_tilemap = $WaterTM
onready var player = $YSort/Player

var rng = RandomNumberGenerator.new()

export(int) var nbTile = 32
export(int) var radiusLake = 3

var CellSize = Vector2(16, 16)
#var width = 1024/CellSize.x
#var height = 1024/CellSize.y
var width = nbTile
var height = nbTile

var grid = []

var Tiles = {
	"empty": -1,
	"wall":2,
	"floor": 3,
	"grass": 0,
	"water" : 1
}

func _init_grid():
	grid = []
	for x in width:
		grid.append([])
		for y in height:
			grid[x].append(-1);

func GetRandomDirection():
	var directions = [[-1, 0], [1, 0], [0, 1], [0, -1]]
	var direction = directions[rng.randi()%4]
	return Vector2(direction[0], direction[1])

func _fillFullGrassToFirstLevel():
	for x in width:		
		for y in height:
			grid[x][y] = Tiles.grass
			
			
func _generateRiver():
	for x in width:
		for y in height:
			if x == y:
				grid[x][y] = Tiles.water
				grid[x-1][y] = Tiles.water
				grid[x][y-1] = Tiles.water
				if x < nbTile-2 || y < nbTile-2:
					if grid[x-1][y-2]== Tiles.water:
						grid[x][y-2] = Tiles.water
					if grid[x-2][y-2]== Tiles.water:
						grid[x-1][y-2] = Tiles.water
						grid[x][y-2] = Tiles.water
					if grid[x+1][y+2]== Tiles.water:
						grid[x][y+2] = Tiles.water
					if grid[x+2][y+2]== Tiles.water:
						grid[x+1][y+2] = Tiles.water
						grid[x][y+2] = Tiles.water
			
func _generateLake():
	var posX = rng.randi_range(0, nbTile)
	var posY = rng.randi_range(0, nbTile)
	grid[posX][posY] = Tiles.water
	for i in radiusLake:
		for j in radiusLake:
			if posX+i < nbTile && posY+j <nbTile:
				grid[posX+i][posY+j] = Tiles.water
			if posX-i > 0 && posY+j <nbTile:
				grid[posX-i][posY+j] = Tiles.water	
			if posX+i <nbTile && posY-j >0:
				grid[posX+i][posY-j] = Tiles.water
			if posX-i > 0 && posY-j >0:
				grid[posX-i][posY-j] = Tiles.water
	
func _create_random_path():
	var max_iterations = 1000
	var itr = 0
	
	var walker = Vector2.ZERO
	
	while itr < max_iterations:
		
		# Perform random walk
		# 1- choose random direction
		# 2- check that direction is in bounds
		# 3- move in that direction
		var random_direction = GetRandomDirection()
		
		if (walker.x + random_direction.x >= 0 and 
			walker.x + random_direction.x < width and
			walker.y + random_direction.y >= 0 and
			walker.y + random_direction.y < height):
				
				walker += random_direction
				grid[walker.x][walker.y] = Tiles.floor
				itr += 1
	
func _spawn_tiles():
	
	for x in width:
		for y in height:
  
			match grid[x][y]:
				Tiles.empty:
					pass
				Tiles.floor:
					dirt_tilemap.set_cellv(Vector2(x, y), 0)
				Tiles.wall:
					wall_tilemap.set_cellv(Vector2(x, y), 0)
				Tiles.grass:
					grass_tilemap.set_cellv(Vector2(x, y),0)
				Tiles.water:
					water_tilemap.set_cellv(Vector2(x, y),0)
					
	dirt_tilemap.update_bitmask_region()
	wall_tilemap.update_bitmask_region()
	grass_tilemap.update_bitmask_region()

func _clear_tilemaps():
	for x in width:
		for y in height:
			dirt_tilemap.clear()
			wall_tilemap.clear()
			grass_tilemap.clear()
			water_tilemap.clear()

func _spawn_player():
	player.position =  Vector2(nbTile/2 * CellSize.x, nbTile/2 * CellSize.y)

func _regenerate():
	rng.randomize()
	_init_grid()
	_clear_tilemaps()
	_fillFullGrassToFirstLevel()
	_create_random_path()
	_generateLake()
	_generateRiver()	
	_spawn_tiles()
	_spawn_player()
	WorldLoadingGeneration.load($CraftedRessourceTM)

func _ready():
	_regenerate()


func _process(delta):
	if Input.is_action_pressed("roll"):
		_regenerate()
