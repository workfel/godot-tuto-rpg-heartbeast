extends Node


func load(tilemap):
	for key in JsonData.world_data:
		var item = JsonData.world_data[key]
		for position in item.positions:
			tilemap.set_cellv(Vector2(position.x, position.y), item.tileId)


func save(key, tileId, x,y):
	if JsonData.world_data[key]:
		if JsonData.world_data[key].tileId == tileId:
			JsonData.world_data[key].positions.append({ "x": x, "y":y})
		else:
			printerr("Mismatch between key & tileId")
	else :
		JsonData.world_data[key] = {
			"tileId" : tileId,
			"positions": [
				{
					"x": x,
					"y": y
				}
			]
		}
	JsonData.saveWorldCraft()
	
