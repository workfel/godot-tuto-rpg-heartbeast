extends Control

var heatlh = 100 setget set_health

onready var progressBar = $TextureProgress

func set_health(value):
	heatlh = clamp(value, 0, 100)
	if progressBar != null:
		progressBar.value = heatlh
	

func _ready():
	self.heatlh = PlayerStats.health
	PlayerStats.connect("health_changed", self, "set_health")
