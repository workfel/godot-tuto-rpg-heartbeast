extends Node

var item_data: Dictionary
var world_data: Dictionary

func _ready():
	item_data = LoadData("res://Data/ItemData.json")
	world_data = LoadData("res://Data/WorldData.json")

func LoadData(file_path):
	var json_data
	var file_data = File.new()
	
	file_data.open(file_path, File.READ)
	json_data = JSON.parse(file_data.get_as_text())
	file_data.close()
	return json_data.result


func saveWorldCraft():
	var save_game = File.new()
	save_game.open("res://Data/WorldData.json", File.WRITE)
	save_game.store_line(to_json(world_data))
	save_game.close()
